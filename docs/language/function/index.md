
Kitsune langauge provides default system level functions for language utility.
These functions 

### functions

Function Name | Function Description
----------- | -------------
[length()](/language/function/length.md) | Returns the length of the string (number of characters) or count of array elements.
[indexOf(string/character)](/language/function/indexof.md) | Returns the zero-based index of the first occurrence of the specified character/string in this current.
[substr(start_index)](/language/function/substr.md) | Retrieves a substring from current string. The substring starts at a specified character position and continues to the end of the string. 
[substr(start_index, length)](/language/function/substr.md) | Retrieves a substring from current string. The substring starts at a specified character position and has a specified length
[toupper()](/language/function/toupper.md) | Returns a copy of current string converted to uppercase.
[tolower()](/language/function/tolower.md) | Returns a copy of current string converted to lowercase.
[urlencode()](/language/function/urlencode.md) | Returns a copy of current string with url encoding. It will remove special characters and replace space with '-'(hyphen)
[split(string)](/language/function/split.md) | Splits a string into array of substrings that are based on the characters/string provided in the input.
[contains(string)](/language/function/contains.md) | Returns true/false based on the input string is present in the current string. it will search case insensitive.
[replace(string, string)](/language/function/replace.md) | Returns a new string in which all occurrences of a specified string in the current instance are replaced with another specified string. 
[decode()](/language/function/decode.md) | Returns a copy of current string by html decoding. 
[view(pagename)](/language/function/view.md) | View function
[partial(pagename)](/language/function/partial.md) | Partial function

