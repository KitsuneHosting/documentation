###Functionality
A URI is represented as a sequence of characters, not as a sequence of octets. That is because URI might be "transported" by means that are not through a computer network, e.g., printed on paper, read over the radio, etc. So all your text, spaces and special characters need to be converted to URLEncoded to be send as a url path way. Kitsune **urlencode()** function enables us to encode the data in url form. 

###Syntax

`[[coffeshop.name.urlencode()]]`

###How does it work?

The `urlencode()` function will return the urlencoded equivalent of the current string

if the value does not exist or null, it will return default null.


###Example
 
coffeeshop(base class) language properties

Type | Name | Value
-----|---- | ----
str | name | sample coffeeshop
str | description | Hello Günter

source
```html
<div>
    <span>[[coffeeshop.description.urlencode()]]</span>
</div>
```

output 
```html
<div>
    <span>hello-g-nter</span>
s</div>
```