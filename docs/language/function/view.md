###Functionality
View enables us to render different views and perform specific view generation related operations. 
It has the following properties which can be invoked along with it to help you create dynamic links. 

#####1. geturl()
#####Functionality
getUrl property can be invoked with view. This will generate the dynamic url of the of the specified link and render it in the HTML. As you can see in the below example we are passing the View a link of 'updates-list.html.dl' which is a k-dl. The getUrl() passes one specific url at a time as you can see here 'updates-1' is passed.
```html
<a class="update-li" href="[[View('/updates-list.html.dl').getUrl()]]" k-get="Get-Url(Updates-List)" k-displayas="link">Updates</a>
```
Output 

```html
<a href="https://braj.nowfloats.com/updates-1">Updates</a>
```

#####2. setObject()
#####Functionality
setObject is used along with view to pass a specific object to the view. For the example below we are passing the object 'Business.products' to the 'product-details' view. As you can see it populates the page with the object that is passed to the view.

```html
<a class="product-overlay" itemprop="url" href="[[View('/products-details.html.dl').setObject(Business.products[a])]]">
             <div class="product-details"><i class="fa fa-search"></i><span>view details</span></div>
</a>
```
Output 
```html
<a class="product-overlay" href="https://braj.nowfloats.com/timex-omg-analog-white-dial-un/p38" itemprop="url">
                 <div class="product-details"><i class="fa fa-search"></i><span>view details</span></div>
</a>
```

#####3. offfset()
#####Functionality
offset can be used along with view to implement pagination in the website. offset can help create pages of specific to your business logic. Here for example we are setting a offset of '8' to show only 8 products per page. 

```html
<div class="wrapper-products" k-repeat="[[Business.products,a,View('/products-list.html.dl').offset:8]]">
</a>
```

