###Functionality

Returns a new string in which all occurrences of a specified string in the current instance are replaced with another specified string. 

###Syntax

`[[coffeeshop.description.replace('&', 'and')]]`

###How does it work?

The `replace(str1,str2)` function will replace all the occurrence of str1 with the str2 in current string and returns the final string.

if the str1 is not present in the current string then it will return str1 as it is.

if the value does not exist or null, it will return default null.


###Example
 
coffeeshop(base class) language properties

Type | Name | Value
-----|---- | ----
str | name | sample coffeeshop
str | description | best place to get your mood fresh & connect

source
```html
<div>
    <!-- this will replace '&' with 'and' form the coffeeshop.description -->
    <span>[[coffeeshop.description.replace('&', 'and')]]</span>
</div>
```

output 
```html
<div>
    <span>best place to get your mood fresh and connect</span>
</div>
```