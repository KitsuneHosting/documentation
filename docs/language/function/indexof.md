###Functionality

Returns the zero-based index of the first occurrence of the specified character/string in current string.

###Syntax

`[[coffeeshop.name.indexof('coffee')]]`

###How does it work?

The indexof(str) function returns the first occurrence of the specified character/string in the current string starting from zero indexes.

if the string is not present in current string datatype it will return -1.

if the value does not exist or null, it will return default -1.


###Example
 
coffeeshop(base class) language properties

Type | Name | Value
-----|---- | ----
str | name | sample coffeeshop

source
```html
<div>
    <!-- this will return the first index of coffee in the coffeeshop.name property -->
    <span>[[coffeeshop.name.indexof('coffee')]]</span>
</div>
```

output 
```html
<div>
    <span>7</span>
</div>
```