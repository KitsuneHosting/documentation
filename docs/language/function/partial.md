###Functionality
Partial offers a way to render a specific common part of a website across all pages reducing redundancy. Let's assume you have a title bar or a specific header or footer, which you want across all the pages, now this can be done with the help of partials feature of Kitsune. 

###Syntax

`[[Partial('/PARTIAL/_HEADER.html')]]`

###How does it work?

Partial will look for the defination of the partial in the given view i.e. '_HEADER.html' in the given path and render the same in the desired location removing code redundancy.

!!!note
    Partial will return a k-partial error if its not defined in the passed HTML file. To find a sample of a partial HTML look for documentation of k-partial.

