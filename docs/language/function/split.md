###Functionality

Splits a string into an array of substrings that are based on the characters/string provided in the input.

###Syntax

`[[coffeeshop.address.split(',')]]`

###How does it work?

The split(str) function returns the array of the string datatype by splitting the current string by the given substring parameter.

if the substring parameter is not present in current string datatype it will return an array with only one element with the value as the current string.

if the value does not exist or null, it will return default null.


###Example
 
coffeeshop(base class) language properties

Type | Name | Value
-----|---- | ----
str | name | sample coffeeshop
str | address | b/11 sample coffeeshop, near mg road, hyderabad, telengana 


source 
```html
<div>
    <!-- this will retun the array by splitting the address by comma(,)  -->
    <ul k-repeat="[[addressline in coffeeshop.address.split(',')]]">
        <li>[[addressline]]</li>
    </ul>
</div>
```


output 
```html
<div>
    <ul>
        <li>b/11 sample coffeeshop</li>
        <li>near mg road</li>
        <li>hyderabad</li>
        <li>telengana</li>
    </ul>
</div>
```