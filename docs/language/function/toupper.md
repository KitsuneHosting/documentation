###Functionality

Returns a copy of this string converted to uppercase.

###Syntax

`[[coffeeshop.description.toupper()]]`

###How does it work?

The `toupper()` function will return the uppercase equivalent of the current string

if the value does not exist or null, it will return default null.


###Example
 
coffeeshop(base class) language properties

Type | Name | Value
-----|---- | ----
str | name | sample coffeeshop
str | description | Best place to get your mood Fresh & Connect

source
```html
<div>
    <span>[[coffeeshop.description.toupper()]]</span>
</div>
```

output 
```html
<div>
    <span>BEST PLACE TO GET YOUR MOOD FRESH & CONNECT</span>
s</div>
```