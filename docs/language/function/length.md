###Functionality

Returns the length of the string (number of characters) or count of array elements.

###Syntax

`[[coffeeshop.name.length()]]`

###How does it work?

The length() function returns the number of characters in the string datatype or the number the elements in array datatype.

if the value does not exist or null, it will return default 0.


###Example
 
coffeeshop(base class) language properties

Type | Name | Value
-----|---- | -----
str | name | sample coffeeshop
array(str) | testimonials | ['very good coffee', 'awesome ambiance']

source 
```html
<div>
    <!-- for str datatyp it will return number of characters -->
    <span>[[coffeeshop.name.length()]]</span>
    <!--for array datatype it will return number of elements present -->
    <span>[[coffeeshop.testimonial.length()]]</span>
</div>
```


output 
```html
<div>
    <span>17</span>
    <span>2</span>
</div>
```