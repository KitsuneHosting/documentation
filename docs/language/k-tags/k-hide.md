#k-hide Tag

Hides the contents if the condition is True.

**Scenario 1:**

To hide the broken image icon, in case there is no image with a blog post.

Using k-hide tag you can simply hide the contents of the div i.e. the ```<img>``` tag.

![Events on coffee theme](/images/tags/events_coffee_theme.JPG)

```
<div class="date">
	<figure><span>20</span>OCT</figure>
</div>
```

Let us change this code to add an img tag here,

```
<div class="date">
	<figure><img src="[[coffeeshop.blogpost[0].blogimage.url]]"></figure>
</div>
```
But this shows the broken image icon :(

```
<div class="date" k-hide="[[coffeeshop.blogpost[0].blogimage.url == '' || coffeeshop.blogpost[0].blogimage.url == null]]">
	<figure><img src="[[coffeeshop.blogpost[0].blogimage.url]]"></figure>
</div>
```
This will hide the image if there is no image with the update or if the url retured by kitsune is null, instead of showing a broken image icon. 

![k-hide with events on coffee theme](/images/tags/k-hide-coffee-theme.JPG)

!!! info
	When kitsune compiles your HTML page, if the condition given with the k-hide tag is true, it removes that html tag with all of its content, thus making it look like the code for the missing information never existed!

**Scenario 2:**

To hide the content if there is no data for a particular section on the website.

***Code example: ***

```
<div k-hide="[[coffeeshop.blogpost.length() == 0]]">
	<img src="[[coffeeshop.blogpost[0].blogimage.url]]">
	<h1>[[coffeeshop.blogpost[0].title]]</h1>
	<p>[[coffeeshop.blogpost[0].description.text]]</p>
</div>
```

This will hide the entire div if there are no blog posts available on k-admin.

!!! info
	Here are some of the operators kitsune supports:

	*, /, +, -,  ==, != , < , >,  <=, >=, &&, ||, !