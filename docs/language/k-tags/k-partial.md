#k-partial tag

It helps you mark the begining of the section which is to be rendered as a partials section. 
k-partial tag in the parent div marks it as the div which is to be rendered as the partial. 

##Example

**When can be used?**

It can be used for any section that is used in multiple pages and stays the same across all of them.

e.g. in /snippets/_analytics.html:
```html hl_lines="1"
<body k-partial="">
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-XXXXXXXX-X"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-XXXXXXX-X');
  </script>
</body>
```

If you notice the html code, the HTML code has a body with a 'k-partial' tag which denotes the section to be a partial. Now when we call this page using partial() we will get the section defined in the k-partial. Thus this can be used to reduce code redundancy. 

**Usage:** in any page
```html hl_lines="5"
<html>
  <head></head>
  <body>
  <p> ... lots of things </p>
  [[Partial('/snippets/_analytics.html')]]
  </body>
</html>
```