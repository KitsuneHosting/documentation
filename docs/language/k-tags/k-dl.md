#k-dl tag

There are two types of URL's:

- ** Static URL** : A statitc URL is one that never changes.

**For example:** ```www.coffeeshop.com/contact-us.html ```

- **Dynamic URL** : A dynamic URL is one which changes according to the contents on a webpage.

**For example:** ```https://manufacturing.com/custom-sheet-metal/p3```

!!! note
	Every dynamic page should have a ```.dl``` extension. For example: ```blog.html.dl```
	
<hr>

**k-dl tag** creates dynamic link for the webpage.

**Code example:** For creating dynamic link of a list page (where all the items are listed)
      
	<head k-dl = "latest-news/[[View('blog.html.dl').currentpagenumber]]">
    	<title> [[coffeeshop.name]] - [[coffeeshop.address.city]] </title>
	</head>

!!! note
	A unique identifier is necessary at the end of the url pattern in the k-dl tag. For example: currentpagenumber, index or id
	
k-dl tag is given in the ```<head>``` tag of HTML, this will generate something similar to

```https://coffeeshophyderabad.com/latest-news/1```

In this case the unique identifier is "1" which is the current page number.

!!! info
	k-dl tag is also used with k-object to create detail pages.