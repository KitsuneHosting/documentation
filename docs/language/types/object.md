### object property type

`object` type is custom datatype in kitsune language. you can create your own class in kitsune language.

- object datatype is used for createing instance of any class
- you can store data in nested format. any property can be type of user defined class.
- using object you can create custom property with class structure
- object can be referenced by value or by referene based on the class definition 

it will be type of userdefined class. you can create your own user defined class and use it to store the data.


##Example
coffeeshop(base class) language properties

Type | Name
-----|----
str | name
geolocaiton | location

geolocation  (userdefined class)

Type | Name
-----|-----
longitude | number
latitude | number

```html
<span>
    <div>
        <iframe width="600" 
                height="450" 
                frameborder="0" 
                style="border:0"
src="https://www.google.com/maps/embed/v1/view?zoom=17&center=[[coffeeshop.location.longitude]],[[coffeeshop.location.latitude]]]&key=..." allowfullscreen></iframe>
    </div>
</span>    
```
