### number property type

The `number` signifies a simple type that stores 64-bit floating-point values. The following table shows the precision and approximate range for the double type.

You can do any numeric operation on this property.

Available operators `+, -, *, /, %, ==, !=`


#Example
coffeeshop(base class) language properties

Type | Name
-----|----
str | name
number | ratings
number | totalTables
number |bookedTables

```html
<span>
    <div>
        Rating : [[coffeeshop.ratings]]
    </div>
    <div>
        Available tables : [[coffeeshop.totalTables - coffeeshop.bookedTables]]
    </div>
</span>    
```