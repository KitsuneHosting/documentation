### boolean property type

The `boolean` type is used to declare variables to store the Boolean values, true and false.

Kitsune expression will return **True/False** based on the place its used.

You can use boolean in the [`k-show`](/language/k-tags/k-show.md) and [`k-hide`](/language/k-tags/k-hide.md) tags.

##Example
coffeeshop(base class) language properties

Type | Name
-----|----
str | name
boolean | wifiAvailable

```html
<!-- just use in the expression -->
<div k-show="[[cofeeshop.wifiAvailable]]">Free Wifi available!</div>

<!-- Display the value -->
<div>[[coffeeshop.isopen]]</div>

```