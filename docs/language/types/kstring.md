### kstring property type

`kstirng` datatype is special datatype in kitsune language.

- kstring is unique datatype of the kitsune language
- using kstring you can store string and kitsune will dynamically extract meaningful keyworeds out of the string stored.
- the keyword extraction of the text is performed dynamically at the time of the text insertion or updating of kstring data type
- keywords extracted from the kstring can be used for better search engine optimization
- kstring datatype has two property (`text` and  `keywords`). `text` is type of [str](/language/types/str.md) and `keywords` is type of [array]




##Example
coffeeshop(baseclass) language properties

Type | Name
-----|----
str | name
kstring | description

kstring (system class)

Type | Name
-----|-----
str | text
array(string) | keywords

```html
<span>
<!-- prints the description text that user added fromt he k-admin -->
<div>[[coffeeshop.desription.text]]</div>
<!-- repeat through all the keywords generated automatilly by the kitsune language -->
    <ul k-repeat="[[keyword in coffeeshop.description.keywords]]">
        <li>[[keyword]]</li>
    </ul>
</span>    
```







