## Kitsune language introduction

**What is kitsune language?**

Kitsune language is the way of creating the data structure and functionalities of your website.

Using kitsune language you can define how you want to store and access the data for the websites, and also you can create customized functionalities.

> before using kitsune language you should have basic knowledge of object orianted programming. You can learn it from 
[here](https://simple.wikipedia.org/wiki/Object-oriented_programming "Basic OOP concepts").

Language definition is the critical part of your dynamic website, make sure you collected all the requirements fo the website.

 * Split requirements in to the modules according to the pages and the main entities.
 * Define the navigation for the website
 * Define the main entities. (i.e.) **product**,  **update**, **offer**, **news** for which you would want saperate details page and list page.
 * Kitsune language is `type safe` language. 
   
    > `"Type safe" usually refers to languages that ensure that an operation is working on the right kind of data at some point before the operation is actually performed.`

Once you have all requirement in place, you can start creating the kitsune language/

To create language start by giving the unique name. this unique name will become the base class of the language.

## Kitsune Class
**What is kitsune class in kitsune?**

Kitsune Class is logical structure of the data that you can use to wrap the relate data fields.

Click [here](/language/class/index.md) to learn more about kitsune class.

## Kitsune Class Property
**What is kitsune class property?**

Kitsune property is a data field that defines the type specific storage and access in kitsune tags and k-admin.

Click [here](/language/types/index.md) to learn more about kitsune class property.