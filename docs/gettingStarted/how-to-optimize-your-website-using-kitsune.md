# How to optimize the website?

**Step 6**. To optimize the files, just select the optimize option available in the drop-down of the project.

![optimize](/images/gettingStarted/optimizeChoose.png)

**Step 7**. A pop-up will appear saying "Optimization Started", which will optimize all the files. Press close in the popup.

![build_started](/images/gettingStarted/optimizeStarted.png)

**Step 8**.  You can preview the website before publishing using the **preview** button present in the bottom row of your project.

![preview_option](/images/gettingStarted/previewChoose.png)

![Website Preview](/images/gettingStarted/previewPage.png)

!!! info
    You can also select **various platforms** for viewing how your webpage will look in mobile or other platforms by selecting it from the top frame.

!!! tip
    You can also send the preview link to the client for approval!
