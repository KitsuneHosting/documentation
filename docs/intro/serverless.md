# Infrastructure
Let's take a peek under the hood to find out why Kitsune is perfect for you.
Kitsune aims to make your website management hassle free. It offers a plethora of out of the box tools which are a headache to implement on your own. Kitsune offers faster load times to worldwide CDN delivery, superior caching and lower bandwidth costs. Multiple layers of security, Guaranteed uptime and Replication across the various zone to ensure that your website never falters. 

![Alt Text](/images/intro/kitsune_advantage.png)
##High Performance 
Serverless websites enable automatic global scale, high availability, and reduction of hosting costs. Kitsune only charge according to your usage. Serverless technology ensures that you don't need to look for handling load or taking up more servers to scale according to your need. Thus automatic global scale, high availability, and reduction of hosting costs is what you achieve with Kitsune.  

Kitsune utilizes **CDN** ensuring that your website has - 

+ Reduced Latency
+ Handles Traffic Spike
+ Creates a cached copy of your site 
+ Your site loads faster from anywhere in the world

## Optimization 

All of this can be achieved with a click of a button. You just need to right click on your project in Kitsune Dashboard and click on optimize. Kitsune will minify all your HTML, CSS and JS i.e. it will remove all the unnecessary characters like line breaks, white spaces etc. It will also apply GZIP compression on your files and provides CDN (Content Delivery Network) services so that your website loads faster from anywhere in the world. If you are wondering what is a CDN, it refers to a geographically distributed group of servers which work together to provide fast delivery of Internet content. A CDN allows for the quick transfer of assets needed for loading Internet content including HTML pages, javascript files, stylesheets, images, and videos.

Oh! Kitsune will also handle versioning to help with file invalidation in case of browser caching.

![Alt Text](/images/intro/optimize_option.png)

###Minification
Minification removes all unnecessary characters from the source code without changing its functionality, making the website fast, reducing resource usage and lowering the bandwidth cost.

###GZIP
GZIP compresses all your pages and other content on the server before sending it to the browser, thus lowering bandwidth costs and giving faster loading websites.

###Versioning
Browser caching creates the problem of file invalidation. If older files are not invalidated regularly then new changes won't be reflected on the website. Kitsune also handles versioning of all your files, so that when a new version is uploaded your browser fetches the new file from the server rather than rendering the cached copy.

## Automatic SEO
![Alt Text](/images/intro/seo.png)

Automatic SEO ensures that your website becomes more relevant and more visible to the world. SEO works in kitsune out of the box. Default server-side rendering & NLP based components enable websites to be auto optimized for SEO.

Kitsune helps you with - 

+ Automated creation of Robots.txt 
+ Smart Keyword extraction
+ Automated sitemap generation
+ Machine Learning based SEO. 






