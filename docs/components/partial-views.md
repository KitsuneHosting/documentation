# Partial views


Partial views help reduce development time by enabling reusability of HTML code which is used across the different pages of the website like the header and footer. 

When the component 'partial views' is enabled, a folder called 'partial' gets added to the project. Under this a default file '_header.html' will be added. 

###Usage
Say you want to reuse the header across your index page and contact us page. 

First, copy the entire section which you want to replicate and paste this in the _header.html file. Now, you will have to ensure that the k-partial tag is used in the first element of the _header.html file(as given in the example below) so that the Kitsune IDE identifies this file as a partial page.  

A sample _header.html file will look something like this : 

```html
<div k-partial>
        all your header code goes in here
</div>
```
You can now reuse this code across all your project HTML files. Instead of the HTML code for your header, use [[Partial('/Partial/_header.html')]] This tag will be replaced during build with the entire HTML code which you placed in the _header.html file.

