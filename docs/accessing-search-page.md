#Accessing the search page

For search, the ```setObject()``` or ```getUrl()``` functions are not used.

When the user clicks on the search button after entering search word, ```onClick``` of the search button, call a JavaScript function which redirects the user to the search page.

**For example:**

The html form for search

```
<form>
	<input type="text" id="searchInput" />
	<input type="button" onClick="goToSearchPage([[Business.rootaliasurl.url]])">
</form>
```
The root url of the site is passes as a parameter to the ```goToSearchPage()``` function.

Your Javascript function ```goToSearchPage()``` will look something like this,

```
function goToSearchPage(rootaliasurl)
{
	var searchInput = $("#searchInput").val();
	if((searchInput == '') || (searchInput == null)){
        		alert(“There is no input”);
	}
	else{
		var SearchUrl = rootaliasurl+"/search/"+searchInput+"/1";
		window.location.href = SearchUrl;
    }
}
```

Here the ```SearchUrl``` will have the url pattern same as what you have defined in the k-dl tag on the search page.

This ```goToSearchPage()``` function will redirect the user to the search page.