# Want to make some changes to your website?

Just click on the "**download**" option present in the menu at the top right of your project.

![download](images/download.png)

Make your changes and just drag and drop the website folder onto the project icon on your dashboard. This will replace all your files with the new files.



![update your website](images/update.jpg)

When all the files have been uploaded successfully, you will get the option of optimizing your website.

![optimize your website](images/upload_success1.png)

optimize your website (if not already done) and preview your changes!

!!! note
    Select the option of "preview (w/o CDN)" otherwise you won't be able to see the changes as preview without CDN will give the latest files, not the cached version.