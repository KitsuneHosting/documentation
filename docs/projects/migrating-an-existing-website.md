# Migrating an Existing Website

### Migrate an already existing website to get the benefits of Kitsune!

Just enter the URL of your website and press "start" button to start the migration.

![1migrate.jpg](/images/projects/crawlWebsiteInput.png)

Kitsune will initialize its fox power to start crawling your website! 

![fox_power](/images/projects/crawlingKitsune0.png)

It will deep crawl your website to find all the links, CSS, JS files and other assets

![analyze_website](/images/projects/crawlingKitsune1.png)

!!! note "Want to know how?"
    Go to http://deepcrawl.getkitsune.com/ to find out more.
 
You can select which links to migrate to Kitsune (if any), then click on Proceed.

![migrate_links](/images/projects/crawlingKitsune2.png)

Kitsune will automatically extract important keywords to improve your site's SEO and place those keywords in the footer of your website

![extracting_keywords](/images/projects/crawlingKitsune3.png)

Then all your files will be imported to the Kitsune platform.

![import_files.png](/images/projects/crawlingKitsune4.png)
        
Next, it will optimize all your files by minifying CSS, JS and also optimizes all the images.

![optimize_files](/images/projects/crawlingKitsune5.png)
        
Next, it will generate Kitsune HTML pages by replacing your older files with new optimized files.

![generate_hmtl.png](/images/projects/crawlingKitsune6.png)
        
When everything is done, you can preview the website before going live, where it also tells you how much faster your website is now.
