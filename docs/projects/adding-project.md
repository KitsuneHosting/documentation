# Adding Projects

When Kitsune dashboard is opened, two options are given, drag your website folder to kitsune or migrate an existing website.

![dahsboard](/images/gettingStarted/folderUpload.png)

You can also click on "create new project" button present at top right on the dashboard and later on add your website files to the project.

![new_project](/images/projects/plusSign.png)

