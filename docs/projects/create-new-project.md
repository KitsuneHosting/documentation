# Create New Project

You can also click on the button to add a new project so that you can later add your files to the project.

![add_new_project_choose](/images/projects/plusSign.png)
![add_new_project](/images/projects/newProjectBlank.png)

When creating a new project button is clicked, you will be prompted to enter a name for your project. Just write any name you want and click on **create and start uploading**.

![name_project](/images/gettingStarted/projectName.png)

After the project is created, just drag and drop your files on the project icon on your dashboard to upload files.