#Properties of a page

Every dynamic page on creation is assigned some properties:

-	pageSize

-	firstPage

-	lastPage

-	previousPage

-	nextPage

-	currentPageNumber

These can used to create paginations or creating dynamic URL's using k-dl tag.

