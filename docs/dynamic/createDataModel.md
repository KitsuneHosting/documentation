# How to create custom Data model using Videos - Walkthrough

## Creating Language

In a new project creating a new **language** is an issue. But this is the stepping stone and also the main part where we need to add many things for the new data mode. This is the root of the whole flow from now on.

!!! note
    The name of the language is fixed and once created cannot be changed for a single project and thus is recommended to keep as same as website project  name.

!!! note
    The name of the language must be unique and thus you would need to try a few times to get a unique language name. This new language name is for you new unique in the whole kitsune platform.

![createLanguage](/gifs/createLanguage.gif)

## Create new Normal Data Type

Data types we can make in the Language can be of generic types like String, Number, Image , Boolean etc . These are the most used Data Types used by all DB and other data storing materials.

![createLanguage](/gifs/newDataType.gif)

## Create a Custom Data Type

Sometimes we need to create data types which cannot be satisfied by just one data type. For example, **address** is not an ordinary data type. It can have many fields like **street name**, **city**, **zipcode** etc. Thus for catering that need we bring to you custom data type . Here you can define your own data type and use them as and when you want.

![createLanguage](/gifs/customDataType.gif)

## Functionalities and Save

Functionalities are similar to any **IDE** and you can see what data types you define, before saving the data model. After you are **finalized** and you are sure you don't wanna change anything anymore go ahead to the save button on the topper right corner of screen and select it for saving your language.

![createLanguage](/gifs/closeAndSave.gif)