<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

#How to use Data Model?

All the classes present in the schema are accessed using the base class. (in our case the Business class)

For example, to render business name in this section:


```
<h3>Welcome to Our <br><span>Coffee Shop</span></h3>    
```
![About Us section of coffee shop theme](/images/dynamic/coffeeshop_about_section.JPG)

Just write,

```
<h3>Welcome to Our<br><span>[[coffeeshop.name]]</span></h3>
```

This will fetch the name of the business from the k-admin of the client.

![Gets business name dynamically](/images/dynamic/business-name-coffee-theme.JPG)

Now change the description of the business,

```
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod ..</p>
```

to
    
```
<p>[[coffeeshop.description]]</p>
```

This will get the business description from the k-admin.

![Gets business description dynamically](/images/dynamic/business-description-coffee-theme.JPG)

So whenever you or the client updates any data on k-admin, it automatically gets updated on the website!

!!! note
    Kitsune returns the naked string as the output, not the encoded string.
    For example: if "Tea & Coffee" is a product name, then the ampersand '&' is returned as '&' instead of '&amp'.

Let's do one more change,

To change the contact number,

```
<h4>+1 (234) 456-789</h4>
```
![Reservation number change](/images/dynamic/reservation_coffee_theme.JPG)

In the schema if you will see the ```contactperson``` is an object of ```contact``` class.
```
<h4>[[coffeeshop.contactperson.primaryno]]</h4>
```

![Reservation number changes dynamically](/images/dynamic/reservation_coffee_theme-dynamic.JPG)

voilà!

!!! info
    Kitsune also comes with inbuilt functions and operators to remove a lot of dependency of JS.