#Creating dynamic websites

Now that you have your static website live, let's make it dynamic!

With kitsune making a website dynamic is so easy that you can do it in just a few hours.

**How?**

**Step 1:** [Open the project in kitsune IDE](#kitsune-ide)

**Step 2:** [Create a schema for your website or load an existing one](/dynamic/data-model.md)

**Step 3:** [Add kitsune tags](/tags/advanced-kitsune-tags.md)

**Step 4:** [Add data in your k-admin]()

And you're all set!

<hr>

##Kitsune IDE

To open your project in kitsune IDE:

**Step 1**: On the kitsune dashboard, click on the top right menu on your project.

![Project](/images/publish/choosePublish.png)

**Step 2**: Select the option i.e. **edit in IDE**. This will open your project in kitsune IDE.

![kitsune IDE](/images/dynamic/kitsuneIde.png)

Looks pretty cool right? ;-)

**Step 3**: After you have changed all the required changes in the **IDE**. Select **Save** button from the top right and save the project.

**Step 4** :  Press Build on the top right corner to build your project and you will see your project been built and ready to be served.

![kitsune IDE](/images/dynamic/buildDone.png)

Looks pretty cool right? ;-)

!!! info
    You can also directly open the kitsune IDE from [ide.kitsune.tools](http://ide.kitsune.tools) and log in using the same GMAIL ID as the one used while signing in onto the dashboard.
