#Website Data Model

Each website has a different data model and kitsune understands it. Hence one can define their own schema and kitsune would optimize and work on the schema that a website is based on. 

A schema is basically made of classes and its properties. Kitsune comes with basic data types number, string, image, link, bool. One can create custom data types too in their schema and kitsune will work using the schema. 

You can create your own schema using the language editor present in the [kitsune IDE](http://ide.kitsune.tools). 

**Basic syntax** that should be followed while creating the schema:

1. The name of the classes and their properties can only have alphanumeric characters (A-Z, a-z, 0-9) and underscore (_). 
2. Don't keep the name of the class and object same.

###Example Schema

```
coffeeshop
{
    string name
    string description
    string facebook
    string twitter
    contact contactperson
    blog blogpost[]
    testimonial review
    featured special[]
    menu section[]
    location address
}
contact
{
    number primaryno
    number fax
}
blog
{
    string title
    image blogimage
    kstring description
}
featured
{
    string name
    string description
    string price
    string discount
}
testimonial
{
    string name
    string description
}
menu
{
    string header
    products product[]
}
products
{
    string name
    string price
}
location
{
    string address1
    string address2
    string country
    string city
    string state
    string pincode
}

```

!!! note
    We'll be using this schema example throughout the documentation for reference :)


Here ```coffeeshop``` is the base class which has properties like ```name```, ```description```, ```email```, ```location```, ```blog```, ```menu``` etc.
Which is the basic information about the business.

Now if you will see ```address``` is an object of the ```location``` class, which consists of some more properties like ```city```, ```state```, ```country``` etc.

```section``` is an object of the menu class which is an array, as there will be multiple sections in a menu and the ```products``` class (object ```product```) has properties ```name```, ```price```, ```description```, ```currencycode```, ```image``` etc. in the menu class

Similarly ```blogpost``` is an object of the ```blog``` class, which again has properties ```title``` and ```description```, where description has data type kstring.


!!! info
    kstring is different from string data type. In kstring you also get auto-generated keywords (for SEO) relevant to the data item stored in that property.