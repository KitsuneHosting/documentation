# How to add Webforms

Click on **"create a webform"** button on the Kitsune dashboard, to start creating your web forms.

![create_webform](images/create_webform.png)
		
You will be redirected to the webform builder page.

![Webform Builder Page](images/webform_home.png)
		
From here you can **drag and drop** the required fields for your form.

![build_webform](images/build_webform.png)

You can **edit the field name, field type** for example: text field, password, email etc. and set max-min length for certain fields also. You can also set the required checkbox to make that field mandatory.

![webform](images/webform.png)

After adding all the fields needed, just click on **save button** to save the form and **fill in the name and description** and click on **next**.

![wenform_save](images/wenform_save.png)
		
Next, **select the existing project** you want to link it to and click on **save**.

![Link to an existing project](images/link_webform.png)

Click on the **Save button** to generate the script or the complete code including html,css and javascript to make the form dynamic.

![js_webform](images/js_webform.png)
		
If you have an already existing form on your website you can just copy paste the JS on your html page containing the webform and call the `submit_bluEnquiryForm()` function in the form.

Sample code is as follows
``` javascript
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
</script>
<script>
    function submit_bluEnquiryForm(name, email, message) {
        var data = {
            'WebsiteId': window.location.href,
            'ActionData': {
                name: name,
                email: email,
                message: message
            }
        }

        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "https://api.kitsune.tools/WebAction/v1/bluEnquiryForm/add-data",
            "method": "POST",
            "processData": false,
            "headers": {
                "content-type": "application/json",
                "authorization": "593ea570ee786c68fc057c01",
            },
            data: JSON.stringify(data),
            success: function(res) {
                console.log(res)
                alert('Successfully saved!')
            },
            error: function() {
                alert('Unable to post. Please try again.')
            }
        });
    }
</script>
```

OR

If you want to create a new form, copy the HTML, CSS and JS and paste it on your HTML page.
Sample code is as follows

```html
<!-- stylesheets -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

<form id="bluEnquiryForm">
    <div class="fb-text form-group field-text-1501157993331">
		<label for="text-1501157993331" class="fb-text-label" name="name">Name<span class="fb-required">*</span></label>
		<input type="text" class="form-control" name="name" id="text-1501157993331" required="required" aria-required="true">
	</div>
    <div class="fb-text form-group field-text-1501158208951">
		<label for="text-1501158208951" class="fb-text-label" name="email">Email<span class="fb-required">*</span></label>
		<input type="email" class="form-control" name="email" id="text-1501158208951" required="required" aria-required="true">
	</div>
    <div class="fb-text form-group field-text-1501158232538">
		<label for="text-1501158232538" class="fb-text-label" name="message">Message<span class="fb-required">*</span></label>
		<input type="text" class="form-control" name="message" id="text-1501158232538" required="required" aria-required="true">
	</div>
</form>
<button type="button" id="wf-button" class="btn btn-default">button</button>
<!-- javascript -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
    function objectifyForm(formArray) { //serialize data function
        var returnArray = {};
        for (var i = 0; i < formArray.length; i++) {
            returnArray[formArray[i]['name']] = formArray[i]['value'];
        }
        return returnArray;
    }
    $('#wf-button').on('click', function() {
        var postObj = objectifyForm($('form#bluEnquiryForm').serializeArray())
        var data = {
            "WebsiteId": window.location.href,
            "ActionData": postObj
        }
        submit(data)
    })

    function submit(data) {
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "https://api.kitsune.tools/WebAction/v1/bluEnquiryForm/add-data",
            "method": "POST",
            "processData": false,
            "headers": {
                "content-type": "application/json",
                "authorization": "593ea570ee786c68fc057c01",
            },
            data: JSON.stringify(data),
            success: function(res) {
                alert('Successfully saved!', res)
            },
            error: function() {
                alert('Unable to post. Please try again.')
            }
        });
    }
</script>
```