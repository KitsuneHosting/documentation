# Welcome to **Kitsune**

Kitsune is **full stack framework** which helps to design a website in a new and interesting way with almost zero learning. Kitsune websites are **fast, discoverable** and can be **easily updated** through a web app.

Kitsune provides a **simple markup language** customizable for any kind of website and gives you the freedom to stop worrying about scalability/ security without writing a single line of code for the backend. Kitsune comes with **SEO** as a package, where it ensures that the content of the website remains the king and **optimizes** all the static assets like JS, CSS and browser caching. It also provides **CDN** for all its assets.

## Why Kitsune?

You can focus on what you do best - **creating great looking websites** - without having to worry about optimizations, buying domains, hosting or SEO.

Kitsune takes care of **buying domains and the hosting**. It also comes along with its own app that your client can use to update their site themselves. It’s as easy as making a social media update. Since it’s easy to update, your clients get more control over their site and update more frequently. Since their content is fresher and more relevant, their websites rank higher on search engines for local queries.

Multiple layers of security to ensure your website is safe. It replicates your data across zone- multiple backups to keep it safe. Guaranteed uptime to ensure your site is always up and running.