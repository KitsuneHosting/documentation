# View Webform Data

From the Kitsune dashboard, click on the **"View forms"** button.

![view_form](images/view_form.png)

This will display all the previously saved forms on your dashboard.

![forms](images/forms.png)

!!! note		
	To edit just click on the **edit button**, make changes and update new design and script of the form on your HTML page.

Click on the **Submissions button** of the form for which you want to view  the submissions.

![View submissions button](images/form_sub_button.png)

!!! note
	You can also see the webforms assigned to a particular project, by selecting the project name from the dropdown available.
	![select project](images/select_project.png)

Download the **csv file** of your submission for viewing offline or other purposes!	

![form_submissions](images/form_submissions.png)

 