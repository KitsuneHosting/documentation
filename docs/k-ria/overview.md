# Reporting and Engagement - Ria

Ria (Relationship Intelligent Agent) is a module in Kitsune which enables reporting and proactive engagement emails to be sent to your customers. You can configure what and when emails should be sent. You also have control over the email template. Edit it the same way you edit HTML pages in Kitsune and you are good to go. Data-points of the reports are provided to you by [Ria schema](#ria-schema-reference).

## Enabling Ria

**Step 1:**

Open your Kitsune project. Under 'MODULES' menu, click on 'ria'. Click 'enable' in the confirmation dialog.

**Step 2:**

Create a new file with name `ria-settings.json` in the root directory of your Kistune project.

Add settings to it based on [Ria settings](#ria-settings-reference). 

**Step 3:**

Design an email template which you want to send as a report. Follow [Ria schema](#ria-schema-reference) to know what ria exposes and how you can access it. Update that email template file name in `ria-settings.json`. 

**Note:**  You can download a ready-to-use performance report email HTML template from <a href="https://riaschemadefault.getkitsune.com/sample-perf-report-template.txt" target="_blank">here</a> and save it as `periodic_performance_report.html` in your project. It makes use of the [Ria schema](#ria-schema-reference) properties to build a nice looking report.  Make sure to update the `ria-settings.json` file with the exact path of this file if you are saving it other than in the project root. 

### Ria settings reference

#### Root object

| Property name            | Date type | Applicable values                    | Description                                                                                                                                   |
|:-------------------------|:----------|:-------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------|
| notifications            | array     | Array of [NotificationItem](#notificationitem-object)    | An array of notifications you want to send to your customers. For details on each notification see notification item reference                |
| settings                 | object    | See [Settings](#settings-object) reference               | Settings to be used by Ria like custom email SMTP details etc.                                                                                |

#### NotificationItem object

| Property name            | Date type | Applicable values                    | Description                                                                                                                                   |
|:-------------------------|:----------|:-------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------|
| type                     | string    | `PERIODIC_PERFORMANCE_REPORT`      | Periodic Performance Report sends information like visits, visitors, top traffic sources and most visited pages periodically to your customers |
| period                   | number    | No. of days                          | Specifies the period of periodic reports in no of days                                                                                        |
| email                    | object    | See [NotificationItem.Email](#notificationitememail-object) reference | Specifies email notification details                                                                                                          |

#### NotificationItem.Email object

| Property name            | Date type | Applicable values                    | Description                                                                                                                                   |
|:-------------------------|:----------|:-------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------|
| body                     | string    | Valid HTML file path                 | The path of HTML template file to be used as email body - relative to the root directory                                                      |

#### Settings object

| Property name            | Date type | Applicable values                    | Description                                                                                                                                   |
|:-------------------------|:----------|:-------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------|
| email                    | object    | See [Settings.Email](#settingsemail-object) reference         | Custom email SMTP details to be used for sending emails                                                                                       |

#### Settings.Email object
| Property name            | Date type | Applicable values                    | Description                                                                                                                                   |
|:-------------------------|:----------|:-------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------|
| host                     | string    | Valid SMTP host                      | The host provided by your email service provider                                                                                              |
| port                     | number    | Valid SMTP port                      | The port provided by your email service provider                                                                                              |
| username                 | string    | Non-empty string                     | The username provided by your email service provider                                                                                          |
| password                 | string    | Non-empty string                     | The password provided by your email service provider                                                                                          |
| ssl_enabled              | boolean   | true/false                           | Enable or disable SSL                                                                                                                         |

Sample `ria-settings.json` for enabling periodic performance reports to be sent to your customers every 7 days.

```json
{
  "notifications": [
      {
          "type": "PERIODIC_PERFORMANCE_REPORT",
          "period": 7,
          "email": {
              "body": "periodic_performance_report.html"
          }
      }
  ],
  "settings": {
      //Fill the details below to use your own email service provider or remove this section to use kitsune's default email service provider
      "email": {
          "host": "",
          "port": 0,
          "username": "",
          "password": "",
          "ssl_enabled": true
      }
  }
}
```

### Ria schema reference

Ria reports get its data from Ria schema. Data like visits, visitors etc is provided to you by Ria schema so that you can use them out of the box and focus only on the design of the email.

All the properties of Ria schema can be accessed using `kapp.ria.<property_name>`

The following are available properties provided by Ria schema :

#### Ria object

| Property name                 | Date type            | Description                                                                                                                                          |
|:------------------------------|:---------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------|
| top_referrers                 | array of [TopReferrersItem](#topreferrersitem-object)     | List of top referrers from which your website has got the traffic in the current window ('Period' in ria-settings.json file defines the window size) |
| top_referrers_prev            | array of [TopReferrersItem](#topreferrersitem-object)     | List of top referrers from which your website has got the traffic in the previous window                                                             |
| most_visited_pages            | array of [MostVisitedPagesItem](#mostvisitedpagesitem-object) | List of the most visited pages in the current window                                                                                                 |
| most_visited_pages_prev       | array of [MostVisitedPagesItem](#mostvisitedpagesitem-object) | List of the most visited pages in the previous window                                                                                                |
| visits                        | number               | Number of visits in current window                                                                                                                   |
| visits_prev                   | number               | Number of visits in the previous window                                                                                                                  |
| visitors                      | number               | Number of visitors in current window                                                                                                                 |
| visitors_prev                 | number               | Number of visitors in the previous window                                                                                                                |
| p_report_dates                | [DateRange](#daterange-object) | The start date and end date of the current window used in performance reports                                                                        |
| p_report_dates_prev           | [DateRange](#daterange-object) | The start date and end date of the previous window used in performance reports                                                                       |
| website_domain                | string               | Domain of the customer                                                                                                                               |
| perf_report_period            | number               | Performance report period in days given in ria-settings.json                                                                                         |
| top_referrers_total           | number               | Total visits received from the top referrers in the current window                                                                                        |
| top_referrers_total_prev      | number               | Total visits received from the top referrers in the previous window                                                                                       |
| most_visited_pages_total      | number               | Total visits received from the most visited pages in the current window                                                                                   |
| most_visited_pages_total_prev | number               | Total visits received from the most visited pages in the previous window                                                                                  |

#### TopReferrersItem object
| Property name                 | Date type            | Description                                                                                                                                          |
|:------------------------------|:---------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------|
| site                          | string               | Referrer host                                                                                                                                        |
| visits                        | number               | No of visits received from this referrer                                                                                                             |

#### MostVisitedPagesItem object
| Property name                 | Date type            | Description                                                                                                                                          |
|:------------------------------|:---------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------|
| page                          | string               | Page URL                                                                                                                                             |
| visits                        | number               | No of visits to this page                                                                                                                            |

#### DateRange object
| Property name                 | Date type            | Description                                                                                                                                          |
|:------------------------------|:---------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------|
| start_date                    | string               | Start date                                                                                                                                           |
| end_date                      | string               | End date                                                                                                                                             |


